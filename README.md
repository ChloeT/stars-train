# Railway to stars

Création d'un jeu JS dans le cadre de la formation dévelopeureuse web chez Simplon



Pour la création de ce jeu, j'ai choisi de partir de zéro et dévelloper individuellement chaque fonctionnalité, afin de m'obliger à aprofondir ma compréhention des bases de ce que j'utilisais. J'ai donc décidé de me baser sur un prompt, que j'aime beaucoup, tout en m'inspirant quelque chose  me semblant réalisable.

*Pitch par UnBotDansLaRam*

![Tu gères un réseau de train entre les étoiles. Il faut gérer les aiguillages et les arrs pour former les bonnes constellations et ammener les passagers à leurs destinations dans les temps ! ](./mediasReadMe/pitch.jpg)



# Organisation du travail



## Gestion du temps 

Du point de vue de l'organisation de mon travail et de la gestion de mon temps par sprints, j'ai choisi dès le départ de diviser le travail comme suit :

- ***Sprint 1*** : du **Lundi 5** au **Vendredi 9**  -  *Travail préparatoire*
- ***Sprint 2*** : du **Lundi 12** au **Mercredi 14**  puis du **Lundi 26** au **Vendredi 30**, entrecoupé par les vancances  -  *Dévelopement des fonctions individuelles*
- ***Sprint 3*** : du **Lundi 2** au **Vendredi 5**  -  *Création du jeu à proprement parler*



Cette répartition s'est révélée plutôt cohérente par la suite et n'a pas nécessité de remise en question.

![Calendrier des sprints](./mediasReadMe/trelloCalendrier.png)





***






## *Sprint 1* : Travail préparatoire 

J'ai choisi de dédier un temps important à cette étape en m'interdisant de commencer à coder. Cela m'a forcé à le détailler au maximum pour éviter de perdre du temps au cours des étapes suivantes.

J'ai commencé par créer la *todo list* des étapes préparatoires. En accord avec le formateur présent, j'ai utilisé *trello* plutôt que les fonctions *board* et *milestones* de *gitLab*. Celui-ci étant plus facile d'utilisation à mon gout et plus riche en possibilités.

Par écrit, j'ai dévellopé le picth du jeu, les mécaniques que j'envisageais, puis ce que je comprenais à cet instant T des fonctionnalités nécessaires. Le tout est stocké dans la carte correspondante de ma liste.



![impr d'écran du trello](./mediasReadMe/blablaTrello.png)



À partir de ce 1er travail, j'ai préparé l'ensemble de ma liste de choses à faire : Les grandes fonctionnalités à coder pour le jeu lui même pour le **sprint 3** et tout ce que j'allais devoir tester individuellement pour le **sprint 2**, les colones *Doing*, *To Review* et *Done*.
J'ai ajouté une colone de *stockage d'informations et d'assets* pour plus de facilité et une meilleure vue d'ensemble et une autre pour *mettre de côté ce qui était accessoire en v1, en vue d'une v2*



![impr d'écran du trello](./mediasReadMe/trello.png)









#### Maquettes

##### 1ere maquette

Uniquement le nécessaire à avoir pour une v1 fonctionnelle. 

![maquette1](./mediasReadMe/maquette1.jpg)



##### 2è maquette

Réalisée lors de la création des assets 

*(travail personnel effectué hors du temps imparti)*

![maquette2](./mediasReadMe/maquette.png)



##### Difficultés rencontrées

Pas de dificultés particulières à cette étape.





***





## *Sprint 2* : Tests de fonctionnalités



Le projet se faisant en parallèle avec les cours, j'ai préféré laisser la construction du jeu à proprement parler pour la fin, de façon à prendre en compte tout ce qui aurait été abordé entre temps. J'ai donc employé ce sprint à tester individuellement les différentes fonctions que je comptais utiliser. Le fait d'avoir un temps plus long pour une phase plus experimentale me semblait une bonne idée, j'ai donc compté en un seul sprint les 3 jours qui précèdaient les vacances et la semaine au retour de celles-ci.



#### Exemples de fonctions testées

##### Compter différentes actions dans une même variable :

```javascript
let count = 0;

let btn1 = document.querySelector('#btn1');
let btn2 = document.querySelector('#btn2');

btn1.addEventListener('click', function () {
    count++;
    console.log(count);
});

btn2.addEventListener('click', function () {
    count++;
    console.log(count);
});
```



##### Une carte à montrer / cacher au clic

```javascript
let myMap = document.querySelector('#showMap');

myMap.addEventListener('click', function () {

    if (myMap.classList.contains('mapClosed')) {
        myMap.classList.remove('mapClosed', "mapAnimatedBack");
        myMap.classList.add('mapOpen', 'mapAnimated');
    }
    else {
        myMap.classList.remove('mapOpen', 'mapAnimated');
        myMap.classList.add('mapClosed', 'mapAnimatedBack');
    }

});
```

***Remarque*** : *j'ai découvert après coup que le CSS*  ` animation-fill-mode: forwards;` *permetait de garder l'affichage de l'état de fin de l'animation, sans devoir appliquer une autre classe CSS pour garder cet état visuel, c'est donc ce que j'ai utilisé dans mon jeu*



##### Ajouter du temps à l'horloge...


```javascript
function addTime(clockHour, clockMinute){
    clockMinute += 5;

    if (clockMinute === 60) {
        clockMinute = 0;
        clockHour++;
    }

    return [clockHour, clockMinute];
}
```



##### ... Et la comparer à l'heure de départ 


```javascript
function compare(clockHour, clockMinute, dueHour, dueMinute) {

    if ((clockHour < dueHour) || (clockHour === dueHour && clockMinute < dueMinute)) {
       return "all fine";        
    }
    
    if (clockHour === dueHour && clockMinute === dueMinute) {
        return "it's time !";
	} 
    
    if ((clockHour > dueHour) || (clockHour === dueHour && clockMinute > dueMinute)) {
        return "you're late.";
    }
}
```





##### Boucler la modification du stade de rotation de l'objet ...


```javascript
let curentRotation = 8;

btn.addEventListener('click', function () {

    if (curentRotation === 8) {
        curentRotation = 1;
    } else {
        curentRotation++;
    }
});
```



##### ... Et le comparer à la position gagnante


```javascript
function IsWinningPosition(theGoodPosition) {
    if (curentRotation === theGoodPosition) {
        return true;
    } else {
        return false; 
    }
}
```



##### Difficultés rencontrées

Le grand souci que j'ai rencontrée à été au niveau de la création de mon compteur. Je voulais un affichage qui fasse défiler les chiffres rapidement lors du lancement de la fonction `addTime()` . Cet élément étant purement esthétique, il aurait été avisé de passer rapidement à autre chose et d'y revenir plus tard si le temps le permettait.





****





## *Sprint 3* : Création du Jeu



Pour ce dernier sprint, il ne restait qu'à créer les elements et assembler les fonctions testées  auparavant.

Comme le sujet abordé en cour à ce moment était les classes JS, j'ai pu commencer directement à coder en les utilisant. Le gros du temps imparti pour ce sprint a été consacré à la compréhention de cette nouvelle notion. Le travail sur le projet n'a pas été très efficace mais formateur sur la facon d'appréhender la POO.

***Remarque*** : *à posteriori, avec les connaissances à disposition à ce moment là, j'ai fait des choix dans l'utilisation et l'organisation des classes qui peuvent paraitre étranges et mériteraient une grosse refonte.*



#### Diagramme UML des classes

![diagramme des classes](./mediasReadMe/UML.png)

***Remarque*** : *N'ayant pas encore compris les facons de lier les class dans le diagramme, il est très possible que celui-ci soit éronné.* 



#### Extraits de code

##### Stars constructor

```javascript
class Stars {
	/**
     * 
     * @param {String} level 
     * @param {Number} starNumber 
     * @param {Number} starX 
     * @param {Number} starY 
     */

    constructor(level, starNumber, starX, starY) {
        this.currentRotation = Math.floor(Math.random() * 8) + 1;
        this.level = level;
        this.starNumber = starNumber;
        this.starX = starX;
        this.starY = starY;
        this.starDraw = null;
        this.isStarSelect = false;
    }
```

`this.currentRotation` ⇨ rotation aléatoire de chaque étoile au chargement du jeu :

![rotation aleatoire](./mediasReadMe/aleatoire.gif)

##### Star.draw() : Récupération de l'adresse de l'image avec les données de l'étoile

```javascript
draw() {

        if (!this.starDraw) {
            this.starDraw = document.createElement('img');
        }

        if (this.starDraw) {
            this.starDraw.innerHTML = '';
        }

        this.starDraw.src = `./medias/${this.level}/stars/star${this.starNumber}.png`
        this.starDraw.style.top = this.starY + 'px';
        this.starDraw.style.left = this.starX + 'px';
        this.starDraw.classList.add('element', 'rotation', 'rotation' + this.currentRotation);

    
        if (this.isStarSelect) {
            this.starDraw.classList.remove('starUnSelect');
        }
        if (!this.isStarSelect) {
            this.starDraw.classList.add('starUnSelect');
        }
    }
```



##### Définition de l'heure de départ du train en fonction des rotations assignées au hasard à chaque partie

```javascript
let currentTime1 = new Clocks(14, 30, 365, 700);
let dueTime1 = new Clocks(14, 30, 640, 112);

let calcDueTime = 3;

for (i = 0; i < starList1.starList.length; i++) {
    console.log(starList1.starList[i].currentRotation);

    if (starList1.starList[i].currentRotation < 5) {
        calcDueTime += starList1.starList[i].currentRotation;
    }
    if (starList1.starList[i].currentRotation === 5) {
        calcDueTime += 3;
    }
    if (starList1.starList[i].currentRotation === 6) {
        calcDueTime += 2;
    }
    if (starList1.starList[i].currentRotation === 7) {
        calcDueTime += 1;
    }
    if (starList1.starList[i].currentRotation === 8) {
        calcDueTime += 0;
    }
}
console.log(calcDueTime);


for (i = 0; i < calcDueTime; i++) {
    dueTime1.addTime();
}
```



##### Fonction StarList.isWin() 

```javascript
isWin() {
        let rightPosition = 0
        for (let i = 0; i < this.starList.length; i++) {
            if (this.starList[i].currentRotation === 8) {
                rightPosition++;
            }
        }

        console.log("il y a " + rightPosition + "étoiles en place")

        if (rightPosition === this.starList.length) {
            return true;

        } else {
            return false;}
    }
```



##### Difficultés rencontrées

Petite difficulté de gestion du temps. Utiliser les notions fraichement apprises impliquant de revoir la façon de penser la structure a pris du temps. Particulièrement avec premiers tatonnements infructueux à cause de fautes de typos et mis de côté un moment en pensant ne pas avoir compris avant d'y revenir une fois le fonctionnement suffisamment saisi pour être capable de débuger. 

Au final, même si les étapes obligatoires que je m'étais fixées ont bien été respectées, et si le résultat est bien un protoptype fonctionnel, j'espérais réussir à ajouter au moins les éléments suivants parmis ceux en option :

- Un message d'accueil expliquant comment jouer 
- Un fond musical
- Un message en plus de l'affichage de la constellation une fois le jeu gagné





## [À venir] Démo



**Démo avec un départ en retard :**

![Gif de démo du jeu](./mediasReadMe/playthrough.gif)



**L'autre message, si le train part à l'heure :**

![message si le train part à l'heure](./mediasReadMe/onTime.gif)

