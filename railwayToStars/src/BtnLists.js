class BtnLists{

    constructor(){
        this.BtnList = [];
        this.elementDraw = null;
    }

    /**
     * 
     * @param {Btns} Btn
     * 
     */
    addBtn(Btn){
        this.BtnList.push(Btn);
        this.draw();
    }


    draw() {

        if(!this.elementDraw){
        this.elementDraw = document.createElement('div');}
        
        if(this.elementDraw){
            this.elementDraw.innerHTML = '';
        }

        for (let i = 0; i < this.BtnList.length; i++) {
            this.BtnList[i].draw();
            this.elementDraw.appendChild(this.BtnList[i].btnDraw);
        }

    }

    
}