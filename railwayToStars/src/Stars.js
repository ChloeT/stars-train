class Stars {
    /**
     *stocker les images et positions en relative de étoiles, une instance par niveau
     */

    /**
     * 
     * @param {String} level 
     * @param {Number} starNumber 
     * @param {Number} starX 
     * @param {Number} starY 
     */

    constructor(level, starNumber, starX, starY) {
        this.currentRotation = Math.floor(Math.random() * 8) + 1;
        this.level = level;
        this.starNumber = starNumber;
        this.starX = starX;
        this.starY = starY;
        this.starDraw = null;
        this.isStarSelect = false;
    }




    draw() {

        if (!this.starDraw) {
            this.starDraw = document.createElement('img');
        }

        if (this.starDraw) {
            this.starDraw.innerHTML = '';
        }

        this.starDraw.src = `./medias/${this.level}/stars/star${this.starNumber}.png`
        this.starDraw.style.top = this.starY + 'px';
        this.starDraw.style.left = this.starX + 'px';
        this.starDraw.classList.add('element', 'rotation', 'rotation' + this.currentRotation);

    
        if (this.isStarSelect) {
            this.starDraw.classList.remove('starUnselect');
        }
        if (!this.isStarSelect) {
            this.starDraw.classList.add('starUnselect');
        }
    }




    selectStar() {
        this.isStarSelect = true;
        this.draw();
        console.log('star' + this.starNumber + 'selected');
    }

    unSelectStar() {
        this.isStarSelect = false;
        this.draw();
    }



    rotate(direction) {

        let rotationClass

        if (direction === "clkW") {

            if (this.currentRotation === 8) {
                this.currentRotation = 1;

            } else {
                this.currentRotation++;
            }

            rotationClass = 'rotation' + this.currentRotation;

        }


        if (direction === "aClkW") {

            rotationClass = 'rotationBack' + this.currentRotation;

            if (this.currentRotation === 1) {
                this.currentRotation = 8;

            } else {
                this.currentRotation--;
            }

        }




        this.starDraw.classList.remove('rotation1', 'rotation2', 'rotation3', 'rotation4', 'rotation5', 'rotation6', 'rotation7', 'rotation8', 'rotationBack1', 'rotationBack2', 'rotationBack3', 'rotationBack4', 'rotationBack5', 'rotationBack6', 'rotationBack7', 'rotationBack8');

        this.starDraw.classList.add(rotationClass);


        console.log('currentRotation : ' + this.currentRotation);
        console.log('rotationClass : ' + rotationClass);
        console.log(this.starDraw);
    }

}
