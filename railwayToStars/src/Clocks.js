class Clocks {

    constructor(hours, minutes, clockX, clockY) {
        this.hours = hours;
        this.minutes = minutes;
        this.clockX = clockX;
        this.clockY = clockY;
        this.clockDraw = null;
    }

  
    addTime() {
        this.minutes += 5;

        if (this.minutes === 60) {
            this.minutes = 0;
            this.hours++;
        }

        this.draw();
    }




    draw() {


        let hoursToDraw;
        let minutesToDraw;


        if (this.hours < 10) {
            hoursToDraw = "0" + this.hours;
        }

        if (this.hours >= 10) {
            hoursToDraw = this.hours;
        }



        if (this.minutes < 10) {
            minutesToDraw = "0" + this.minutes;
        }

        if (this.minutes >= 10) {
            minutesToDraw = this.minutes;
        }



        if (!this.clockDraw) {
            this.clockDraw = document.createElement('p');
        }

        if (this.clockDraw) {
            this.clockDraw.innerHTML = '';
        }

        this.clockDraw.textContent = `${hoursToDraw} : ${minutesToDraw}`;
        this.clockDraw.style.top = this.clockY + 'px';
        this.clockDraw.style.left = this.clockX + 'px';
        this.clockDraw.classList.add('element');


    }


    /**
     * 
     * @param {Clocks} DueTime 
     */
    isLate(DueTime) {

        if ((this.hours < DueTime.hours) || (this.hours === DueTime.hours && this.minutes < DueTime.minutes)) {
            return "all fine";


        } else if (this.hours === DueTime.hours && this.minutes === DueTime.minutes) {
            return "it's time !";


        } else if ((this.hours > DueTime.hours) || (this.hours === DueTime.hours && this.minutes > DueTime.minutes)) {
            return "you're late.";

        } else {
            return "urk";
        }
    }


}



