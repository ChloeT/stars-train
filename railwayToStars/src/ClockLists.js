class ClockLists{

    /**
     * 
     * @param {string} level 
     */
    constructor(level){
        this.level = level;
        this.clockList = [];
        this.elementDraw = null;
    }

    /**
     * 
     * @param {Clocks} clock
     * 
     */
    addClock(clock){
        this.clockList.push(clock);
        this.draw();
    }


    draw() {

        if(!this.elementDraw){
        this.elementDraw = document.createElement('div');}
        
        if(this.elementDraw){
            this.elementDraw.innerHTML = '';
        }

        for (let i = 0; i < this.clockList.length; i++) {
            this.clockList[i].draw();
            this.elementDraw.appendChild(this.clockList[i].clockDraw);
        }

    }

    
}