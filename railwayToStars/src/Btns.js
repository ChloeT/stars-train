class Btns {

    /**
     * 
     * @param {Boolean} isClockWise 
     * @param {Number} btnX 
     * @param {Number} btnY 
     */
    constructor(isClockWise, btnX, btnY) {
        this.isClockWise = isClockWise
        this.btnX = btnX;
        this.btnY = btnY;
        this.btnDraw = null;
    }




    draw() {
        if (!this.btnDraw) {
            this.btnDraw = document.createElement('img');
        }

        if (this.btnDraw) {
            this.btnDraw.innerHTML = '';
        }


        if (this.isClockWise) {
            this.btnDraw.src = './medias/btnClkW.png'
        }

        if (!this.isClockWise) {
            this.btnDraw.src = './medias/btnAClkW.png'
        }

        this.btnDraw.style.top = this.btnY + 'px';
        this.btnDraw.style.left = this.btnX + 'px';
        this.btnDraw.classList.add('element');
    }

}




