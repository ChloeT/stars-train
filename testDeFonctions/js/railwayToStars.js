let canvas = document.querySelector('#game');
let ctx = canvas.getContext('2d');


let star1= new Image();
star1.src = "./medias/etoile1.png";

star1.onload = function(){
    ctx.drawImage(star1, 90, 108);
}

let star2= new Image();
star2.onload = function(){
    ctx.drawImage(star2, 253, 153);
}
star2.src = "./medias/etoile2.png";

let star3= new Image();
star3.onload = function(){
    ctx.drawImage(star3, 316, 246);
}
star3.src = "./medias/etoile3.png";

let star4= new Image();
star4.onload = function(){
    ctx.drawImage(star4, 406, 351);
}
star4.src = "./medias/etoile4.png";

let star5= new Image();
star5.onload = function(){
    ctx.drawImage(star5, 650, 451);
}
star5.src = "./medias/etoile5.png";

let star6= new Image();
star6.onload = function(){
    ctx.drawImage(star6, 561, 556);
}
star6.src = "./medias/etoile6.png";

let star7= new Image();
star7.onload = function(){
    ctx.drawImage(star7, 379, 463);
}
star7.src = "./medias/etoile7.png";


let btnRotateClockwise = new Image();
btnRotateClockwise.onload = function(){
    ctx.drawImage(btnRotateClockwise, 110, 680);
}
btnRotateClockwise.src = "./medias/bouttonHoraire.png";

let btnRotateAntiClockwise = new Image();
btnRotateAntiClockwise.onload = function(){
    ctx.drawImage(btnRotateAntiClockwise, 594, 680);
}
btnRotateAntiClockwise.src = "./medias/bouttonHoraire.png";