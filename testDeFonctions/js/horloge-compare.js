// recup des emplacements

let hereDueHour = document.querySelector('#dueHour');
let hereDueMinute = document.querySelector('#dueMinute');
let hereHours = document.querySelector('#hours');
let hereMinutes = document.querySelector('#minutes');
let add5Btn = document.querySelector('#add5min');
let message = document.querySelector('#message');


// décla variables
let dueHourOfTheLevel = 14;
let dueMinuteOfTheLevel = 10;


let curentHourOfTheLevel = 14;
let curentMinOfTheLevel = 00;



// apl des fct

showDueTime(dueHourOfTheLevel,dueMinuteOfTheLevel,hereDueHour,hereDueMinute);
showCurrentTime(curentHourOfTheLevel, curentMinOfTheLevel, hereHours, hereMinutes);


add5Btn.addEventListener('click', function () {
    addTime(curentHourOfTheLevel, curentMinOfTheLevel);

    let result = addTime(curentHourOfTheLevel, curentMinOfTheLevel);
    curentHourOfTheLevel = result[0];
    curentMinOfTheLevel = result[1];

    showCurrentTime(curentHourOfTheLevel, curentMinOfTheLevel, hereHours, hereMinutes);
    compare(curentHourOfTheLevel,dueHourOfTheLevel, curentMinOfTheLevel, dueMinuteOfTheLevel);
});




// fct

function showDueTime(dueHourValue, dueMinuteValue, dueHour, dueMinute) {

    
    if (dueHourValue < 10) {
        dueHour.textContent = "0" + dueHourValue;
    } else {
        dueHour.textContent = dueHourValue;
    }

    if (dueMinuteValue < 10) {
        dueMinute.textContent = "0" + dueMinuteValue;
    } else {
        dueMinute.textContent = dueMinuteValue;
    }
}



function showCurrentTime(currentTimeHours, currentTimeMinutes, horlogeHours, horlogeMinutes) {
    if (currentTimeHours < 10) {
        horlogeHours.textContent = "0" + currentTimeHours;
    } else {
        horlogeHours.textContent = currentTimeHours;
    }

    if (currentTimeMinutes < 10) {
        horlogeMinutes.textContent = "0" + currentTimeMinutes;
    } else {
        horlogeMinutes.textContent = currentTimeMinutes;
    }
}




function addTime(currentTimeHours, currentTimeMinutes){
    currentTimeMinutes += 5;

    if (currentTimeMinutes === 60) {
        currentTimeMinutes = 0;
        currentTimeHours++;
    }

    return [currentTimeHours, currentTimeMinutes];
}







function compare(currentTimeHours, dueHourValue, currentTimeMinutes, dueMinuteValue) {
    console.log(currentTimeHours, dueHourValue, currentTimeMinutes, dueMinuteValue);
    
    let showMe;


    if ((currentTimeHours < dueHourValue) || (currentTimeHours === dueHourValue && currentTimeMinutes < dueMinuteValue)) {
        showMe = "all fine";
        console.log("all fine");
        

    } else if (currentTimeHours === dueHourValue && currentTimeMinutes === dueMinuteValue) {
        showMe = "it's time !";
        console.log("it's time !");
        

    } else if ((currentTimeHours > dueHourValue) || (currentTimeHours === dueHourValue && currentTimeMinutes > dueMinuteValue)) {
        showMe = "you're late.";
        console.log("you're late.");

    }else{
        showMe ="urk";
        console.log("urk");
    }

    message.textContent = showMe;
}

